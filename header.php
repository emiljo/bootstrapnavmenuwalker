<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<?php
				
				$args = array(
					'theme_location' => 'top-bar',
					'depth'		 => 0,
					'container'	 => false,
					'menu_class'	 => 'nav',
					'walker'	 => new BootstrapNavMenuWalker()
				);

				wp_nav_menu($args);
			
			?>
		</div>
	</div>
</div>